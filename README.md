Personal QMK Centromere keymap
==============================

This is my personal Centromere keyboard keymap. Feel free to use it if you find
it useful.

Make sure to symlink `config.h` and `rules.mk` under the `centromere_plus` dir.

To upload, run:

```
make centromere_plus:stavros:avrdude
```
