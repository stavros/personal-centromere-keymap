#include "centromere_plus.h"

enum telophase_layers {
    _L1,
    _LT,
    _LW,
    _NUM,
    _L2,
    _PRG,
    _GME,
};

enum centromere_keycodes {
    L1 = SAFE_RANGE,
    L2,

    KC_LSPM,
    KC_RSPM,

    // Autotypes.
    MSTK,
    SIGN,
    NAME,
    EMAL,
    MAIN,
    SLOW
};

enum unicode_names {
    BANG,
    IRONY,
    SNEK
};

const uint32_t PROGMEM unicode_map[] = {
    [BANG]  = 0x203D,  // ‽
    [IRONY] = 0x2E2E,  // ⸮
    [SNEK]  = 0x1F40D, // 🐍
};

// Reimplementing Space Cadet here to hopefully fix some stuff.
static bool shift_interrupted[2] = {0, 0};
static uint16_t scs_timer[2] = {0, 0};

bool init_run = false;

#define XXX   KC_NO
#define ___   KC_TRNS
#define TODO  KC_TRNS  // A keycode so I remember to remap the key.

#define KC_DQT LSFT(KC_QUOT)
#define BIG LCAG(KC_NO)
#define ESCTRL MT(MOD_LCTL, KC_ESC)
#define LT_LW LT(_LW, KC_DEL)
#define LT_NUM LT(_NUM, KC_GRV)
#define LT_PRG LT(_PRG, KC_TAB)

#ifndef LSPO_KEY
#define LSPO_KEY KC_9
#endif
#ifndef RSPC_KEY
#define RSPC_KEY KC_0
#endif


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_L1] = {
        { LT_NUM,   KC_1,     KC_2,     KC_3,     KC_4,     KC_5,     TG(_GME),         KC_EQL,   KC_6,     KC_7,     KC_8,     KC_9,     KC_0,     KC_MINS  },
        { LT_PRG,   KC_Q,     KC_W,     KC_E,     KC_R,     KC_T,     TODO,             KC_LBRC,  KC_Y,     KC_U,     KC_I,     KC_O,     KC_P,     KC_RBRC  },
        { ESCTRL,   KC_A,     KC_S,     KC_D,     KC_F,     KC_G,     TODO,             KC_BSLS,  KC_H,     KC_J,     KC_K,     KC_L,     KC_SCLN,  KC_QUOT  },
        { KC_LSPM,  KC_Z,     KC_X,     KC_C,     KC_V,     KC_B,     XXX,              XXX,      KC_N,     KC_M,     KC_COMM,  KC_DOT,   KC_SLSH,  KC_RSPM  },
        { XXX,      XXX,      KC_LWIN,  KC_LALT,  LT_LW,    KC_BSPC,  XXX,              XXX,      KC_SPC,   BIG,      KC_ENT,   MO(_L2),  XXX,      XXX      }
    },

    [_NUM] = {  // Numpad.
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      KC_NLCK,  KC_PSLS,  KC_PAST,  KC_PMNS,  ___,      ___      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      KC_P7,    KC_P8,    KC_P9,    KC_PPLS,  ___,      ___      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      KC_P4,    KC_P5,    KC_P6,    KC_PPLS,  ___,      ___      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      KC_P1,    KC_P2,    KC_P3,    KC_PENT,  ___,      ___      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      KC_P0,    ___,      KC_P0,    KC_PDOT,  ___,      ___      },
    },

    [_LW] = {  // Various autotypes.
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      X(IRONY), X(SNEK),  X(BANG),  SPAM,     ___,      ___      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      SIGN,     NAME,     EMAL,     MAIN,     ___,      ___      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
    },

    [_LT] = {  // Laptop keymap which replaces ESCTRL with Caps Lock.
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
        { KC_CAPS,  ___,      ___,      ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
    },

    [_L2] = {  // Function keys etc.
        { ___,      KC_F1,    KC_F2,    KC_F3,    KC_F4,    KC_F5,    KC_F6,            KC_F7,    KC_F8,    KC_F9,    KC_F10,   KC_F11,   KC_F12,   ___      },
        { ___,      KC_PWR,   KC_PSCR,  ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
        { ___,      KC_MUTE,  KC_VOLD,  KC_VOLU,  ___,      ___,      ___,              ___,      KC_LEFT,  KC_DOWN,  KC_UP,    KC_RIGHT, ___,      ___      },
        { ___,      KC_MPRV,  KC_MPLY,  KC_MSTP,  KC_MNXT,  ___,      ___,              ___,      KC_INS,   KC_HOME,  KC_END,   KC_PGUP,  KC_PGDN,  ___      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
    },

    [_PRG] = {  // Programming layer.
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              XXX,      XXX,      XXX,      XXX,      XXX,      XXX,      XXX      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              XXX,      XXX,      KC_UNDS,  KC_EQL,   KC_BSLS,  KC_PIPE,  XXX      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              XXX,      XXX,      KC_LCBR,  KC_RCBR,  KC_LBRC,  KC_RBRC,  KC_DQT   },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              XXX,      XXX,      KC_PLUS,  XXX,      XXX,      XXX,      XXX      },
        { ___,      ___,      ___,      ___,      ___,      ___,      ___,              XXX,      XXX,      XXX,      XXX,      XXX,      XXX,      XXX      },
    },

    [_GME] = {  // Game layer.
        { KC_ESC,   ___,      ___,      ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
        { KC_TAB,   ___,      ___,      ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
        { KC_LCTL,  ___,      ___,      ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
        { KC_LSFT,  ___,      ___,      ___,      ___,      ___,      ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
        { ___,      ___,      ___,      ___,      KC_GRV,   KC_SPC,   ___,              ___,      ___,      ___,      ___,      ___,      ___,      ___      },
    },

};

// If modifier is pressed, send keycode.
/*
static bool special_mods(uint16_t keycode, keyrecord_t *record, uint16_t modifier) {
    if (record->event.pressed && (get_mods() & MOD_BIT(modifier))) {
        register_code(keycode);
        return false;
    } else {
        unregister_code(keycode);
        return true;
    }
}
*/


bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    if (!init_run) {
        // Set the emoji output type to Linux-compatible on first run.
        set_unicode_input_mode(UC_LNX);
        init_run = true;
    }

    if (record->event.pressed) {
        switch (keycode) {
        case NAME:
            SEND_STRING("Stavros Korokithakis");
            return false;
        case SIGN:
            SEND_STRING("\n\nThanks,\nStavros");
            return false;
        case EMAL:
            SEND_STRING("hi@stavros.io");
            return false;
        case SPAM:
            SEND_STRING("@mail.stavros.io");
            return false;
        case MAIN:
            SEND_STRING("if __name__ == \"__main__\":\n");
            return false;
        }
    }

    switch (keycode) {
    // Enable the Laptop layer with Ctrl+Alt+Shift+Super+dash.
    case KC_MINUS: {
        if (record->event.pressed && (get_mods() == (MOD_BIT(KC_LSHIFT) | MOD_BIT(KC_LALT) | MOD_BIT(KC_LCTRL) | MOD_BIT(KC_LWIN)))) {
            layer_on(_LT);
            return false;
        }
        break;
    }

    case KC_RSPM: {
        if (record->event.pressed) {
            if (get_mods() & MOD_BIT(KC_LSHIFT)) {
                shift_interrupted[0] = true;
                shift_interrupted[1] = true;
                SEND_STRING("()");
            } else {
                shift_interrupted[1] = false;
                scs_timer[1] = timer_read();
                register_mods(MOD_BIT(KC_RSFT));
            }
        } else {
            if (get_mods() & MOD_BIT(KC_LSFT)) {
                shift_interrupted[0] = true;
                shift_interrupted[1] = true;
            }
            if (!shift_interrupted[1] && timer_elapsed(scs_timer[1]) < TAPPING_TERM) {
                register_code(RSPC_KEY);
                unregister_code(RSPC_KEY);
            }
            unregister_mods(MOD_BIT(KC_RSFT));
        }
        return false;
    }

    case KC_LSPM: {
        if (record->event.pressed) {
            if (get_mods() & MOD_BIT(KC_RSHIFT)) {
                shift_interrupted[0] = true;
                shift_interrupted[1] = true;
                register_code(KC_CAPS);
            } else if (get_mods() & MOD_BIT(KC_LALT)) {
                // The if check here is because the keyboard would send a "9" whenever I pressed Alt-Shift,
                // for some reason.
                shift_interrupted[0] = true;
                shift_interrupted[1] = true;
                register_mods(MOD_BIT(KC_LSFT));
            } else {
                shift_interrupted[0] = false;
                scs_timer[0] = timer_read();
                register_mods(MOD_BIT(KC_LSFT));
            }
        } else {
            if (get_mods() & MOD_BIT(KC_RSFT)) {
                shift_interrupted[0] = true;
                shift_interrupted[1] = true;
            }
            if (!shift_interrupted[0] && timer_elapsed(scs_timer[0]) < TAPPING_TERM) {
                register_code(LSPO_KEY);
                unregister_code(LSPO_KEY);
            }
            unregister_mods(MOD_BIT(KC_LSFT));
            unregister_code(KC_CAPS);
        }
        return false;
    }

    }

    // If none of the above keys are pressed, or if they don't have a return statement in their body,
    // we probably pressed a normal key, and should interrupt Space Cadet and handle the key.
    shift_interrupted[0] = true;
    shift_interrupted[1] = true;
    return true;
}
